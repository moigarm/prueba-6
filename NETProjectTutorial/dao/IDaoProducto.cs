﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoProducto : IDao<Producto>
    {
        Producto findByNombre(string nombre);
        Producto findBySku(string sku);
    }
}
