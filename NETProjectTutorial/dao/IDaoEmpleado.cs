﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoEmpleado : IDao<Empleado>
    {
        Empleado findById(string Id);
        List<Empleado> findByLastname(string lastname);
        Empleado findByCedula(string cedula);
    }
}
