﻿using NETProjectTutorial.entities;
using NETProjectTutorial.implements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> ListClientes = new List<Cliente>();
        private implements.DaoImplementsCliente daoCliente;

        public ClienteModel()
        {
            daoCliente = new implements.DaoImplementsCliente();
        }

        public List<Cliente> GetListCliente()
        {
            return daoCliente.findAll();
        }

        public void Populate()
        {
            ListClientes = JsonConvert.DeserializeObject<List<Cliente>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Cliente_data));

            foreach (Cliente c in ListClientes)
            {
                daoCliente.save(c);
            }
        }

        public void save(DataRow cliente)
        {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(cliente["Id"].ToString());
            c.Cedula = cliente["Cédula"].ToString();
            c.Nombres = cliente["Nombre"].ToString();
            c.Apellidos = cliente["Apellidos"].ToString();
            c.Telefono = cliente["Teléfono"].ToString();
            c.Correo = cliente["Correo"].ToString();
            c.Direccion = cliente["Dirección"].ToString();

            daoCliente.save(c);
        }

        public void update(DataRow cliente)
        {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(cliente["Id"].ToString());
            c.Cedula = cliente["Cédula"].ToString();
            c.Nombres = cliente["Nombre"].ToString();
            c.Apellidos = cliente["Apellidos"].ToString();
            c.Telefono = cliente["Teléfono"].ToString();
            c.Correo = cliente["Correo"].ToString();
            c.Direccion = cliente["Dirección"].ToString();

            daoCliente.update(c);
        }

        public void delete(DataRow cliente)
        {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(cliente["Id"].ToString());
            c.Cedula = cliente["Cédula"].ToString();
            c.Nombres = cliente["Nombre"].ToString();
            c.Apellidos = cliente["Apellidos"].ToString();
            c.Telefono = cliente["Teléfono"].ToString();
            c.Correo = cliente["Correo"].ToString();
            c.Direccion = cliente["Dirección"].ToString();

            daoCliente.delete(c);
        }
    }
}
